/*
 * Copyright (c) 2022 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <device.h>
#include <drivers/display.h>
#include <init.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zephyr.h>

#include "cats.h"

#include <logging/log.h>
LOG_MODULE_REGISTER(cats_main, CONFIG_CATS_LOG_LEVEL);

struct main_screen {
	lv_obj_t *obj;
	lv_obj_t *label;
};

static struct main_screen main_scr;
static sys_dlist_t screen_list_hor = SYS_DLIST_STATIC_INIT(&screen_list_hor);
static sys_dlist_t screen_list_ver = SYS_DLIST_STATIC_INIT(&screen_list_ver);
static K_MUTEX_DEFINE(screen_lock);
static struct cats_screen *active_screen; // NULL if main is the active screen

LV_IMG_DECLARE(oniro_icon);

static struct cats_screen *to_cats_screen(sys_dlist_t *node)
{
	return CONTAINER_OF(node, struct cats_screen, dnode);
}

static void load_screen(struct cats_screen *scr)
{
	LOG_DBG("Loading screen: %s", scr->name);

	lv_scr_load(scr->obj);
	active_screen = scr;
}

static void load_main(void)
{
	LOG_DBG("Loading main screen");

	lv_scr_load(main_scr.obj);
	active_screen = NULL;
}

static void gesture_event_cb(lv_event_t *event)
{
	struct cats_screen *scr;
	sys_dlist_t *node = NULL;

	lv_dir_t dir = lv_indev_get_gesture_dir(lv_indev_get_act());

	k_mutex_lock(&screen_lock, K_FOREVER);

	switch (dir) {
	case LV_DIR_LEFT:
		LOG_DBG("Left swipe");
		if (!active_screen) {
			node = sys_dlist_peek_head(&screen_list_hor);
		} else {
			if (active_screen->orientation == CATS_SCREEN_VERTICAL)
				break;

			node = sys_dlist_peek_next_no_check(&screen_list_hor,
							&active_screen->dnode);
		}
		break;
	case LV_DIR_RIGHT:
		LOG_DBG("Right swipe");
		if (active_screen) {
			if (active_screen->orientation == CATS_SCREEN_VERTICAL)
				break;

			node = sys_dlist_peek_prev_no_check(&screen_list_hor,
							&active_screen->dnode);
			if (!node) {
				load_main();
			}
		}
		break;
	case LV_DIR_TOP:
		LOG_DBG("Top swipe");
		if (!active_screen) {
			node = sys_dlist_peek_head(&screen_list_ver);
		} else {
			if (active_screen->orientation == CATS_SCREEN_HORIZONTAL)
				break;

			node = sys_dlist_peek_next_no_check(&screen_list_ver,
							&active_screen->dnode);
		}
		break;
	case LV_DIR_BOTTOM:
		LOG_DBG("Bottom swipe");
		if (active_screen) {
			if (active_screen->orientation == CATS_SCREEN_HORIZONTAL)
				break;

			node = sys_dlist_peek_prev_no_check(&screen_list_ver,
							&active_screen->dnode);
			if (!node) {
				load_main();
			}
		}
		break;
	default:
		break;
	}

	if (node) {
		scr = to_cats_screen(node);
		load_screen(scr);
	}

	k_mutex_unlock(&screen_lock);
}

static void init_gesture(lv_obj_t *obj)
{
	lv_obj_add_event_cb(obj, gesture_event_cb, LV_EVENT_GESTURE, NULL);
}

void cats_add_screen(struct cats_screen *scr)
{
	init_gesture(scr->obj);
	k_mutex_lock(&screen_lock, K_FOREVER);
	switch (scr->orientation) {
	case CATS_SCREEN_VERTICAL:
		sys_dlist_append(&screen_list_ver, &scr->dnode);
		break;
	default:
		sys_dlist_append(&screen_list_hor, &scr->dnode);
		break;
	}
	k_mutex_unlock(&screen_lock);
}

static void start_main_screen(void)
{
	lv_obj_t *label, *img;

	LOG_DBG("Creating main screen");

	main_scr.obj = lv_obj_create(NULL);

	label = lv_label_create(main_scr.obj);
	lv_label_set_text(label, "Oniro CATS");
	lv_obj_align(label, LV_ALIGN_TOP_MID, 0, 30);

	init_gesture(main_scr.obj);

	img = lv_img_create(main_scr.obj);
	lv_img_set_src(img, &oniro_icon);
	lv_obj_align(img, LV_ALIGN_CENTER, 0, 0);
	lv_obj_set_size(img, 230, 230);

	load_main();
}

void main(void)
{
	const struct device *display_dev;

	display_dev = DEVICE_DT_GET(DT_CHOSEN(zephyr_display));
	if (!device_is_ready(display_dev)) {
		LOG_ERR("Device not ready, aborting test");
		return;
	}

	lv_task_handler();
	display_blanking_off(display_dev);
	start_main_screen();

	LOG_INF("Context-Aware Touch Screen app ready");

	while (1) {
		lv_task_handler();
		k_sleep(K_MSEC(1));
	}
}
