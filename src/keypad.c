/*
 * Copyright (c) 2022 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <init.h>

#include "cats.h"

#include <logging/log.h>
LOG_MODULE_REGISTER(cats_keypad, CONFIG_CATS_LOG_LEVEL);

static struct cats_screen scr;

LV_IMG_DECLARE(key_0);
LV_IMG_DECLARE(key_1);
LV_IMG_DECLARE(key_2);
LV_IMG_DECLARE(key_3);
LV_IMG_DECLARE(key_4);
LV_IMG_DECLARE(key_5);
LV_IMG_DECLARE(key_6);
LV_IMG_DECLARE(key_7);
LV_IMG_DECLARE(key_8);
LV_IMG_DECLARE(key_9);
LV_IMG_DECLARE(key_asterisk);
LV_IMG_DECLARE(key_hash);

static lv_style_t pressed_style;

#define NUM_ROWS 4
#define NUM_COLS 3

static const lv_coord_t col_dsc[] = {
	LV_GRID_FR(1),
	LV_GRID_FR(1),
	LV_GRID_FR(1),
	LV_GRID_TEMPLATE_LAST
};

static const lv_coord_t row_dsc[] = {
	LV_GRID_FR(1),
	LV_GRID_FR(1),
	LV_GRID_FR(1),
	LV_GRID_FR(1),
	LV_GRID_TEMPLATE_LAST
};

static const lv_img_dsc_t *key_imgs[NUM_ROWS][NUM_COLS] = {
	{ &key_7, &key_8, &key_9 },
	{ &key_4, &key_5, &key_6 },
	{ &key_1, &key_2, &key_3 },
	{ &key_asterisk, &key_0, &key_hash },
};

static int register_keypad_screen(const struct device *unused)
{
	lv_obj_t *keypad, *btn;
	int i, j;

	LOG_DBG("Creating the keypad screen");

	lv_style_init(&pressed_style);
	lv_style_set_img_recolor_opa(&pressed_style, LV_OPA_30);
	lv_style_set_img_recolor(&pressed_style, lv_color_white());
	
	scr.name = "keypad";
	scr.obj = lv_obj_create(NULL);
	scr.orientation = CATS_SCREEN_HORIZONTAL;

	keypad = lv_obj_create(scr.obj);
	lv_obj_set_style_grid_column_dsc_array(keypad, col_dsc, 0);
	lv_obj_set_style_grid_row_dsc_array(keypad, row_dsc, 0);
	lv_obj_set_size(keypad, 240, 320);
	lv_obj_set_layout(keypad, LV_LAYOUT_GRID);
	lv_obj_align(keypad, LV_ALIGN_CENTER, 0, 0);

	for (i = 0; i < NUM_ROWS; i++) {
		for (j = 0; j < NUM_COLS; j++) {
			btn = lv_imgbtn_create(keypad);
			lv_imgbtn_set_src(btn, LV_IMGBTN_STATE_RELEASED,
					  NULL, key_imgs[i][j], NULL);
			lv_obj_add_style(btn, &pressed_style, LV_STATE_PRESSED);
			lv_obj_set_size(btn, 64, 64);
			lv_obj_set_grid_cell(btn, LV_GRID_ALIGN_STRETCH, j, 1,
					     LV_GRID_ALIGN_STRETCH, i, 1);
		}
	}

	cats_add_screen(&scr);

	return 0;
}
CATS_SCREEN_INIT(register_keypad_screen);
